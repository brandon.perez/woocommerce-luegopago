<?php
  /**
   * Controlador REST API de Productos
   *
   * Maneja los request al endpoint /products
   *
   * @package LuegopaGo\RestApi
   *
   */

   defined( 'ABSPATH' ) || exit;
  /**
   * Clase del controlador REST API de Productos
   *
   * @package LuegopaGo\RestApi
   * @extends
   */
  class LPG_REST_Products_Controller extends WC_REST_Controller {

    /**
     * Namespace del endpoint.
     *
     * @var string
     */
    protected $namespace = 'luegopago';

    /**
     * Base de ruta.
     *
     * @var string
     */
    protected $rest_base = 'products';

    /**
     * Registrar rutas para productos.
     */
    public function register_routes() {
      register_rest_route(
        $this->namespace, '/' . $this->rest_base,
        array(
          array(
            'methods'             => 'POST',
            'callback'            => array( $this, 'create_product' ),
            'permission_callback' => array( $this, 'create_item_permissions_check' ),
          ),
          array(
            'methods'             => 'GET',
            'callback'            => array( $this, 'get_product' ),
          ),
        ),
      );

      register_rest_route(
        $this->namespace, '/' . $this->rest_base . '/userId=(?P<userId>\d+)',
        array(
          'methods' => 'GET',
          'callback' => array(),
          'permission_callback' => array(),
        ),
      );
    }

    private function get_product() {
      $args = array(
        'post_type' => 'product'
      );
      $query = new WP_Query($args);
      if ( $query->have_posts() ) {
        $query->the_post();
        $products = array();
        foreach($query->get_post() as $post) {
          $product = new WC_Product($post->ID);
          $products[] = $product->get_data();
        }
        return $products;
      } else {
        return array();
      }
    }

  }

?>
<?php
/**
 * Woocommerce LuegopaGo Rest API
 *
 * @package           LuegopaGo\RestApi
 * @copyright         2021 LuegopaGo
 *
 * @wordpress-plugin
 * Plugin Name:       WooCommerce LuegopaGo
 * Plugin URI:        https://luegopago.com/
 * Description:       Rest API de LuegopaGo con integración de Woocommerce.
 * Version:           2.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            LuegopaGo
 * Author URI:        https://luegopago.com
 * Text Domain:       wcl_rest_api
 */

  namespace LuegopaGo\RestApi;

  defined( 'ABSPATH' ) || exit;

  /**
   * Validar que Woocomerce esté activado.
   */
  if ( ! function_exists( 'is_woocommerce_activated' ) ) {
    function is_woocommerce_activated() {
      if ( !class_exists( 'woocommerce' ) ) exit;
    }
  }

  /**
   * Clase responsable de cargar la API REST de LuegopaGo.
   */
  class LuegopaGoServer {

    /**
     * Base de REST API.
     *
     * @var string
     */
    protected $namespace = 'luegopago';

    /**
     * Endpoints de la REST API.
     *
     * @var array
     */
    protected $controllers = array();

    /**
     * Inicialización requerida del Hook de Wordpress para la API REST.
     */
    public function init() {

      add_action( 'woocommerce_loaded', function () {

        // Controladores de la API REST usando el namespace luegopago.
        add_action( 'rest_api_init', array( $this, 'register_rest_routes' ) );

        // Autenticar usuario si ingresa las claves de la API.
        add_action( 'woocommerce_rest_is_request_to_rest_api', array( $this, 'authenticate_user' ) );

      }, 10 );

    }

    /**
     * Registro de rutas de la API REST.
     */
    public function register_rest_routes() {
      foreach ( $this->get_rest_namespaces() as $namespace => $controllers ) {
        foreach ( $controllers as $controller_name => $controller_class ) {
          $this->controllers[ $namespace ][ $controller_name ] = new $controller_class();
          $this->controllers[ $namespace ][ $controller_name ]->register_routes();
        }
      }
    }

    /**
     * Registrar namespace.
     *
     * @return array Namespaces y principales clases.
     */
    protected function get_rest_namespace() {
      return apply_filters( 'woocommerce_rest_api_get_rest_namespaces',
        array(
          'luegopago' => $this->get_controllers(),
        )
      );
    }

    /**
     * Lista de clases del namespace luegopago.
     *
     * @return array
     */
    protected function get_controllers() {
      return array(
        'products' => 'LPG_REST_Products_Controller',
        'orders' => 'LPG_REST_Orders_Controller',
        'categories' => 'LPG_REST_Categories_Controller',
        'attributes' => 'LPG_REST_Attributes_Controller',
      );
    }

    /**
     * Autenticar si el usuario usa WWPP Rest Base si se usan las claves de API.
     *
     * @param bool $rest_request
     * @return bool
     */
    public function authenticate_user() {
      $rest_prefix = trailingslashit(rest_get_url_prefix());
      $request_uri = esc_url_raw(wp_unslash($_SERVER['REQUEST_URI']));

      if ((false !== strpos($request_uri, $rest_prefix . 'luegopago/'))) {
          return true;
      }

      return $rest_request;
    }

  }

?>